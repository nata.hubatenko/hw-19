function willTeamMeetDeadline(speeds, backlog, deadline) {
	const workDays = getWorkDaysUntilDeadline(deadline);
	const totalPoints = backlog.reduce((acc, curr) => acc + curr, 0);
	const totalSpeed = speeds.reduce((acc, curr) => acc + curr, 0);
	const workPoints = totalSpeed * workDays * 8;

	if (workPoints >= totalPoints) {
		const daysLeft = getDaysUntilDeadline(deadline);
		const message = `Усі завдання будуть успішно виконані за ${daysLeft} днів до настання дедлайну!`;
		console.log(message);
	} else {
		const additionalHours = Math.ceil((totalPoints - workPoints) / totalSpeed);
		const message = `Команді розробників доведеться витратити додатково ${additionalHours} годин після дедлайну, щоб виконати всі завдання в беклозі`;
		console.log(message);
	}
}

function getWorkDaysUntilDeadline(deadline) {
	const now = new Date();
	const timeDiff = deadline.getTime() - now.getTime();
	const workDays = Math.floor(timeDiff / (1000 * 60 * 60 * 24 * 7)) * 5;
	const dayDiff = deadline.getDay() - now.getDay();
	if (dayDiff > 0 && dayDiff <= 5) {
		workDays += dayDiff;
	} else if (dayDiff > 5) {
		workDays += 5;
	}
	return workDays;
}

function getDaysUntilDeadline(deadline) {
	const now = new Date();
	const timeDiff = deadline.getTime() - now.getTime();
	const daysLeft = Math.ceil(timeDiff / (1000 * 60 * 60 * 24));
	return daysLeft;
}